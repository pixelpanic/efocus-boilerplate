module.exports = function(grunt){

  'use strict';
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    notify: {
      compass: {
        options: {
          title: 'Build',
          message: 'Compass and Concatenate done',
        }
      },
      assets: {
        options: {
          title: 'Assets',
          message: 'Concatenate and uglify done'
        }
      }
    },

    compass: {
      main: {
        options: {
          bundleExec: true,
          config: 'compass.rb',
        },
        files: {
          'build/css/application.css': [
            'assets/scss/application.scss'
          ]
        }
      },
    },

    cssmin: {
      build: {
        src: 'build/css/application.css',
        dest: 'build/css/application.css'
      }
    },

    watch: {
      js: {
        files: ['assets/js/application.js'],
        tasks: ['concat']
      },
      compass: {
        files: ['assets/scss/*/*.scss'],
        tasks: ['compass'],
        options: {
          livereload: true,
          sourcemap: true
        }
      },
    },

    concat: {
      dev: {
        src: [
          'assets/js/*.js',
        ],
        dest: 'build/js/application.js'
      }
    },

    bower_concat: {
      all: {
        dest: 'build/js/assets.js'
      }
    },

    clean: ['build/js/application.js', 'build/css/application.css'],

    uglify: {
      build: {
        files: {
          'build/js/application.js': ['build/js/application.js']
        }
      },
      assets: {
        files: {
          'build/js/assets.js': ['build/js/assets.js']
        }
      }
    }

  });

  grunt.registerTask('default', ['build', 'notify:compass']);
  grunt.registerTask('build', ['compass', 'concat']);
  grunt.registerTask('assets', ['bower_concat', 'uglify:assets', 'notify:assets']);
  grunt.registerTask('production', ['compass', 'cssmin', 'bower_concat', 'uglify:assets', 'uglify']);

};