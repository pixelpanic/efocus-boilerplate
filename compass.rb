# Breakpoint is used for writing super easy media queries
# http://breakpoint-sass.com/

require 'breakpoint'


# Compass settings
# http://compass-style.org/help/tutorials/configuration-reference/

sass_dir = "assets/scss" # Original folder for scss files
css_dir = "build/css" # Destination folder for compiled css
images_dir = "build/images" # Original folder for images
generated_images_dir = "build/images/generated" # Folder where Compass puts processed image (eg: sprites)
javascripts_dir = "assets/js" # Javascripts folder
fonts_dir = "build/fonts" # Fonts folder

relative_assets = true # Relative assets boolean, if false you should set a http_path (default: "/")
output_style = :expanded # Output style for compiled css (:nested, :expanded, :compact, or :compressed)
line_comments = true # Boolean for line comments that indicate where the selectors are defined